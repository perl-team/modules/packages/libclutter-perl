libclutter-perl (1.110-7) UNRELEASED; urgency=medium

  * debian/watch: use uscan version 4.

 -- gregor herrmann <gregoa@debian.org>  Sat, 22 Feb 2020 16:36:41 +0100

libclutter-perl (1.110-6) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ intrigeri ]
  * Declare compliance with Debian Policy 4.2.1.
  * Bump debhelper compatibility level to 11.
  * Remove obsolete upstream VCS URLs.

 -- intrigeri <intrigeri@debian.org>  Mon, 29 Oct 2018 19:30:11 +0000

libclutter-perl (1.110-5) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Ryan Niebur from Uploaders. Thanks for your work!

  [ intrigeri ]
  * Remove duplicate word in debian/changelog.
  * Bump debhelper compat level to 10 and adjust build-dependencies accordingly.
  * Declare compliance with Standards-Version 4.0.0: no change required.

 -- intrigeri <intrigeri@debian.org>  Sat, 24 Jun 2017 11:27:09 +0000

libclutter-perl (1.110-4) unstable; urgency=medium

  * Rename autopkgtest configuration file(s) as per new pkg-perl-
    autopkgtest schema.
  * Convert package to arch:all. It contains only arch-independent
    code. (Closes: #809399)
  * Update years of packaging copyright.

 -- gregor herrmann <gregoa@debian.org>  Thu, 31 Dec 2015 01:49:51 +0100

libclutter-perl (1.110-3) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ intrigeri ]
  * Add debian/upstream/metadata
  * Add upstream Git repository information to debian/upstream/metadata.
  * Declare compliance with Standards-Version 3.9.6.
  * Mark package as autopkgtest-able, but disable upstream test
    that relies on DRI2 etc., which we have not in adt-run environment.
  * Stop installing the empty upstream NEWS file.
  * Update upstream and packaging copyright information.
  * Override the Lintian no-upstream-changelog check: upstream ships no
    such thing anymore.

 -- intrigeri <intrigeri@debian.org>  Sat, 23 May 2015 18:37:36 +0200

libclutter-perl (1.110-2) unstable; urgency=low

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ intrigeri ]
  * Bump debhelper compat level to 9.
  * Declare compatibility with Standards-Version 3.9.5.
  * Reformat debian/control with cme fix dpkg.

 -- intrigeri <intrigeri@debian.org>  Sun, 09 Mar 2014 02:12:30 +0100

libclutter-perl (1.110-1) unstable; urgency=low

  [ gregor herrmann ]
  * Use source format 3.0 (quilt).
  * Run tests under xvfb.
  * Remove debian/source/local-options; abort-on-upstream-changes
    and unapply-patches are default in dpkg-source since 1.16.1.

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ intrigeri ]
  * Imported Upstream version 1.110 (Closes: #620936)
  * Drop pod-fixes.patch: patched .xs files are not shipped by the new
    introspection-based library anymore.
  * Refresh dependencies.
  * Update debian/copyright to today's standards, thanks to cme.
  * Disable dh_auto_test: the only test case segfaults in pbuilder + xvfb-run.
    Note that it passes flawlessly inside a regular X session.
  * Add myself to uploaders.
  * Bump standards version to 3.9.3 (no change required).

 -- intrigeri <intrigeri@debian.org>  Sun, 03 Jun 2012 11:15:41 +0200

libclutter-perl (1.002-1) unstable; urgency=low

  [ Ryan Niebur ]
  * Take over for the Debian Perl Group; Closes: #560886 -- RFA
  * debian/control: Added: Vcs-Svn field (source stanza); Vcs-Browser
    field (source stanza); Homepage field (source stanza). Changed:
    Maintainer set to Debian Perl Group <pkg-perl-
    maintainers@lists.alioth.debian.org> (was: Ross Burton
    <ross@debian.org>)
  * rename source package to libclutter-perl
  * New upstream release
    - fixes docs (Closes: #499355)
    - Keysyms.pm isn't empty (LP: 222207)
  * Add myself to Uploaders
  * set Standards-Version to 3.8.3
  * refresh

  [ gregor herrmann ]
  * debian/control: Changed: Replace versioned (build-)dependency on
    perl (>= 5.6.0-{12,16}) with an unversioned dependency on perl (as
    permitted by Debian Policy 3.8.3).
  * debian/watch: switch to CPAN.
  * New upstream release.
  * Add /me to Uploaders.
  * Fix hashbang in
  * Add a patch to fix several POD issues; add quilt framework.

 -- gregor herrmann <gregoa@debian.org>  Mon, 21 Dec 2009 23:33:49 +0100

clutter-perl (0.8.0.1-1) unstable; urgency=low

  * New upstream release.
  * Bump Clutter depends to 0.8 and add missing depends

 -- Ross Burton <ross@debian.org>  Tue, 22 Jul 2008 09:19:03 +0100

clutter-perl (0.6.0.0-1) unstable; urgency=low

  * New upstream release (Closes: #467573).

 -- Ross Burton <ross@debian.org>  Tue, 26 Feb 2008 21:05:15 +0000

clutter-perl (0.4.1-2) unstable; urgency=low

  * Add missing dependency on libgtk2-perl (Closes: #450671)

 -- Ross Burton <ross@debian.org>  Fri,  9 Nov 2007 09:39:55 +0000

clutter-perl (0.4.1-1) unstable; urgency=low

  * New upstream release.

 -- Ross Burton <ross@debian.org>  Wed,  8 Aug 2007 14:53:38 +0100

clutter-perl (0.2.0-3) unstable; urgency=low

  * Don't run the tests (Closes: #414895)

 -- Ross Burton <ross@debian.org>  Wed, 14 Mar 2007 16:22:25 +0000

clutter-perl (0.2.0-2) unstable; urgency=low

  * Fix nested examples directory.

 -- Ross Burton <ross@debian.org>  Sun, 11 Mar 2007 13:53:07 +0000

clutter-perl (0.2.0-1) unstable; urgency=low

  * Initial release.

 -- Ross Burton <ross@debian.org>  Wed,  7 Feb 2007 16:44:39 +0000
